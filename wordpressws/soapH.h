/* soapH.h
   Generated by gSOAP 2.7.15 from WordPressWSN2P.h
   Copyright(C) 2000-2009, Robert van Engelen, Genivia Inc. All Rights Reserved.
   This part of the software is released under one of the following licenses:
   GPL, the gSOAP public license, or Genivia's license for commercial use.
*/

#ifndef soapH_H
#define soapH_H
#include "soapStub.h"
#ifndef WITH_NOIDREF

#ifdef __cplusplus
extern "C" {
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_markelement(struct soap*, const void*, int);
SOAP_FMAC3 int SOAP_FMAC4 soap_putelement(struct soap*, const void*, const char*, int, int);
SOAP_FMAC3 void *SOAP_FMAC4 soap_getelement(struct soap*, int*);

#ifdef __cplusplus
}
#endif
SOAP_FMAC3 int SOAP_FMAC4 soap_putindependent(struct soap*);
SOAP_FMAC3 int SOAP_FMAC4 soap_getindependent(struct soap*);
#endif
SOAP_FMAC3 int SOAP_FMAC4 soap_ignore_element(struct soap*);

SOAP_FMAC3 void * SOAP_FMAC4 soap_instantiate(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 int SOAP_FMAC4 soap_fdelete(struct soap_clist*);
SOAP_FMAC3 void* SOAP_FMAC4 soap_class_id_enter(struct soap*, const char*, void*, int, size_t, const char*, const char*);

#ifndef SOAP_TYPE_byte
#define SOAP_TYPE_byte (3)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_byte(struct soap*, char *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_byte(struct soap*, const char*, int, const char *, const char*);
SOAP_FMAC3 char * SOAP_FMAC4 soap_in_byte(struct soap*, const char*, char *, const char*);

#define soap_write_byte(soap, data) ( soap_begin_send(soap) || (soap_serialize_byte(soap, data), 0) || soap_put_byte(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_byte(struct soap*, const char *, const char*, const char*);

#define soap_read_byte(soap, data) ( soap_begin_recv(soap) || !soap_get_byte(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 char * SOAP_FMAC4 soap_get_byte(struct soap*, char *, const char*, const char*);

#ifndef SOAP_TYPE_int
#define SOAP_TYPE_int (1)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_int(struct soap*, int *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_int(struct soap*, const char*, int, const int *, const char*);
SOAP_FMAC3 int * SOAP_FMAC4 soap_in_int(struct soap*, const char*, int *, const char*);

#define soap_write_int(soap, data) ( soap_begin_send(soap) || (soap_serialize_int(soap, data), 0) || soap_put_int(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_int(struct soap*, const int *, const char*, const char*);

#define soap_read_int(soap, data) ( soap_begin_recv(soap) || !soap_get_int(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 int * SOAP_FMAC4 soap_get_int(struct soap*, int *, const char*, const char*);

#ifndef SOAP_TYPE_ns1__responseData
#define SOAP_TYPE_ns1__responseData (8)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns1__responseData(struct soap*, const char*, int, const ns1__responseData *, const char*);
SOAP_FMAC3 ns1__responseData * SOAP_FMAC4 soap_in_ns1__responseData(struct soap*, const char*, ns1__responseData *, const char*);

#define soap_write_ns1__responseData(soap, data) ( soap_begin_send(soap) || ((data)->soap_serialize(soap), 0) || (data)->soap_put(soap, NULL, NULL) || soap_end_send(soap) )


#define soap_read_ns1__responseData(soap, data) ( soap_begin_recv(soap) || !soap_get_ns1__responseData(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 ns1__responseData * SOAP_FMAC4 soap_get_ns1__responseData(struct soap*, ns1__responseData *, const char*, const char*);

#define soap_new_ns1__responseData(soap, n) ::soap_instantiate_ns1__responseData(soap, n, NULL, NULL, NULL)


#define soap_delete_ns1__responseData(soap, p) soap_delete(soap, p)

SOAP_FMAC1 ns1__responseData * SOAP_FMAC2 soap_instantiate_ns1__responseData(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns1__responseData(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns1__sendData
#define SOAP_TYPE_ns1__sendData (7)
#endif

SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns1__sendData(struct soap*, const char*, int, const ns1__sendData *, const char*);
SOAP_FMAC3 ns1__sendData * SOAP_FMAC4 soap_in_ns1__sendData(struct soap*, const char*, ns1__sendData *, const char*);

#define soap_write_ns1__sendData(soap, data) ( soap_begin_send(soap) || ((data)->soap_serialize(soap), 0) || (data)->soap_put(soap, NULL, NULL) || soap_end_send(soap) )


#define soap_read_ns1__sendData(soap, data) ( soap_begin_recv(soap) || !soap_get_ns1__sendData(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 ns1__sendData * SOAP_FMAC4 soap_get_ns1__sendData(struct soap*, ns1__sendData *, const char*, const char*);

#define soap_new_ns1__sendData(soap, n) ::soap_instantiate_ns1__sendData(soap, n, NULL, NULL, NULL)


#define soap_delete_ns1__sendData(soap, p) soap_delete(soap, p)

SOAP_FMAC1 ns1__sendData * SOAP_FMAC2 soap_instantiate_ns1__sendData(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns1__sendData(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Fault
#define SOAP_TYPE_SOAP_ENV__Fault (23)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Fault(struct soap*, struct SOAP_ENV__Fault *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Fault(struct soap*, const struct SOAP_ENV__Fault *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Fault(struct soap*, const char*, int, const struct SOAP_ENV__Fault *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Fault * SOAP_FMAC4 soap_in_SOAP_ENV__Fault(struct soap*, const char*, struct SOAP_ENV__Fault *, const char*);

#define soap_write_SOAP_ENV__Fault(soap, data) ( soap_begin_send(soap) || (soap_serialize_SOAP_ENV__Fault(soap, data), 0) || soap_put_SOAP_ENV__Fault(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Fault(struct soap*, const struct SOAP_ENV__Fault *, const char*, const char*);

#define soap_read_SOAP_ENV__Fault(soap, data) ( soap_begin_recv(soap) || !soap_get_SOAP_ENV__Fault(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct SOAP_ENV__Fault * SOAP_FMAC4 soap_get_SOAP_ENV__Fault(struct soap*, struct SOAP_ENV__Fault *, const char*, const char*);

#define soap_new_SOAP_ENV__Fault(soap, n) ::soap_instantiate_SOAP_ENV__Fault(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Fault(soap, p) soap_delete(soap, p)

SOAP_FMAC1 struct SOAP_ENV__Fault * SOAP_FMAC2 soap_instantiate_SOAP_ENV__Fault(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Fault(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Reason
#define SOAP_TYPE_SOAP_ENV__Reason (22)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Reason(struct soap*, const struct SOAP_ENV__Reason *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Reason(struct soap*, const char*, int, const struct SOAP_ENV__Reason *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Reason * SOAP_FMAC4 soap_in_SOAP_ENV__Reason(struct soap*, const char*, struct SOAP_ENV__Reason *, const char*);

#define soap_write_SOAP_ENV__Reason(soap, data) ( soap_begin_send(soap) || (soap_serialize_SOAP_ENV__Reason(soap, data), 0) || soap_put_SOAP_ENV__Reason(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Reason(struct soap*, const struct SOAP_ENV__Reason *, const char*, const char*);

#define soap_read_SOAP_ENV__Reason(soap, data) ( soap_begin_recv(soap) || !soap_get_SOAP_ENV__Reason(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct SOAP_ENV__Reason * SOAP_FMAC4 soap_get_SOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason *, const char*, const char*);

#define soap_new_SOAP_ENV__Reason(soap, n) ::soap_instantiate_SOAP_ENV__Reason(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Reason(soap, p) soap_delete(soap, p)

SOAP_FMAC1 struct SOAP_ENV__Reason * SOAP_FMAC2 soap_instantiate_SOAP_ENV__Reason(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Reason(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Detail
#define SOAP_TYPE_SOAP_ENV__Detail (19)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Detail(struct soap*, const struct SOAP_ENV__Detail *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Detail(struct soap*, const char*, int, const struct SOAP_ENV__Detail *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Detail * SOAP_FMAC4 soap_in_SOAP_ENV__Detail(struct soap*, const char*, struct SOAP_ENV__Detail *, const char*);

#define soap_write_SOAP_ENV__Detail(soap, data) ( soap_begin_send(soap) || (soap_serialize_SOAP_ENV__Detail(soap, data), 0) || soap_put_SOAP_ENV__Detail(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Detail(struct soap*, const struct SOAP_ENV__Detail *, const char*, const char*);

#define soap_read_SOAP_ENV__Detail(soap, data) ( soap_begin_recv(soap) || !soap_get_SOAP_ENV__Detail(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct SOAP_ENV__Detail * SOAP_FMAC4 soap_get_SOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail *, const char*, const char*);

#define soap_new_SOAP_ENV__Detail(soap, n) ::soap_instantiate_SOAP_ENV__Detail(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Detail(soap, p) soap_delete(soap, p)

SOAP_FMAC1 struct SOAP_ENV__Detail * SOAP_FMAC2 soap_instantiate_SOAP_ENV__Detail(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Detail(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Code
#define SOAP_TYPE_SOAP_ENV__Code (17)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Code(struct soap*, const struct SOAP_ENV__Code *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Code(struct soap*, const char*, int, const struct SOAP_ENV__Code *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Code * SOAP_FMAC4 soap_in_SOAP_ENV__Code(struct soap*, const char*, struct SOAP_ENV__Code *, const char*);

#define soap_write_SOAP_ENV__Code(soap, data) ( soap_begin_send(soap) || (soap_serialize_SOAP_ENV__Code(soap, data), 0) || soap_put_SOAP_ENV__Code(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Code(struct soap*, const struct SOAP_ENV__Code *, const char*, const char*);

#define soap_read_SOAP_ENV__Code(soap, data) ( soap_begin_recv(soap) || !soap_get_SOAP_ENV__Code(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct SOAP_ENV__Code * SOAP_FMAC4 soap_get_SOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code *, const char*, const char*);

#define soap_new_SOAP_ENV__Code(soap, n) ::soap_instantiate_SOAP_ENV__Code(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Code(soap, p) soap_delete(soap, p)

SOAP_FMAC1 struct SOAP_ENV__Code * SOAP_FMAC2 soap_instantiate_SOAP_ENV__Code(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Code(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_SOAP_ENV__Header
#define SOAP_TYPE_SOAP_ENV__Header (16)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_SOAP_ENV__Header(struct soap*, struct SOAP_ENV__Header *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_SOAP_ENV__Header(struct soap*, const struct SOAP_ENV__Header *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_SOAP_ENV__Header(struct soap*, const char*, int, const struct SOAP_ENV__Header *, const char*);
SOAP_FMAC3 struct SOAP_ENV__Header * SOAP_FMAC4 soap_in_SOAP_ENV__Header(struct soap*, const char*, struct SOAP_ENV__Header *, const char*);

#define soap_write_SOAP_ENV__Header(soap, data) ( soap_begin_send(soap) || (soap_serialize_SOAP_ENV__Header(soap, data), 0) || soap_put_SOAP_ENV__Header(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_SOAP_ENV__Header(struct soap*, const struct SOAP_ENV__Header *, const char*, const char*);

#define soap_read_SOAP_ENV__Header(soap, data) ( soap_begin_recv(soap) || !soap_get_SOAP_ENV__Header(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct SOAP_ENV__Header * SOAP_FMAC4 soap_get_SOAP_ENV__Header(struct soap*, struct SOAP_ENV__Header *, const char*, const char*);

#define soap_new_SOAP_ENV__Header(soap, n) ::soap_instantiate_SOAP_ENV__Header(soap, n, NULL, NULL, NULL)


#define soap_delete_SOAP_ENV__Header(soap, p) soap_delete(soap, p)

SOAP_FMAC1 struct SOAP_ENV__Header * SOAP_FMAC2 soap_instantiate_SOAP_ENV__Header(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_SOAP_ENV__Header(struct soap*, int, int, void*, size_t, const void*, size_t);

#endif

#ifndef SOAP_TYPE_ns1__sendPost
#define SOAP_TYPE_ns1__sendPost (15)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_ns1__sendPost(struct soap*, struct ns1__sendPost *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_ns1__sendPost(struct soap*, const struct ns1__sendPost *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns1__sendPost(struct soap*, const char*, int, const struct ns1__sendPost *, const char*);
SOAP_FMAC3 struct ns1__sendPost * SOAP_FMAC4 soap_in_ns1__sendPost(struct soap*, const char*, struct ns1__sendPost *, const char*);

#define soap_write_ns1__sendPost(soap, data) ( soap_begin_send(soap) || (soap_serialize_ns1__sendPost(soap, data), 0) || soap_put_ns1__sendPost(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_ns1__sendPost(struct soap*, const struct ns1__sendPost *, const char*, const char*);

#define soap_read_ns1__sendPost(soap, data) ( soap_begin_recv(soap) || !soap_get_ns1__sendPost(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct ns1__sendPost * SOAP_FMAC4 soap_get_ns1__sendPost(struct soap*, struct ns1__sendPost *, const char*, const char*);

#define soap_new_ns1__sendPost(soap, n) ::soap_instantiate_ns1__sendPost(soap, n, NULL, NULL, NULL)


#define soap_delete_ns1__sendPost(soap, p) soap_delete(soap, p)

SOAP_FMAC1 struct ns1__sendPost * SOAP_FMAC2 soap_instantiate_ns1__sendPost(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns1__sendPost(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef SOAP_TYPE_ns1__sendPostResponse
#define SOAP_TYPE_ns1__sendPostResponse (10)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_ns1__sendPostResponse(struct soap*, struct ns1__sendPostResponse *);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_ns1__sendPostResponse(struct soap*, const struct ns1__sendPostResponse *);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_ns1__sendPostResponse(struct soap*, const char*, int, const struct ns1__sendPostResponse *, const char*);
SOAP_FMAC3 struct ns1__sendPostResponse * SOAP_FMAC4 soap_in_ns1__sendPostResponse(struct soap*, const char*, struct ns1__sendPostResponse *, const char*);

#define soap_write_ns1__sendPostResponse(soap, data) ( soap_begin_send(soap) || (soap_serialize_ns1__sendPostResponse(soap, data), 0) || soap_put_ns1__sendPostResponse(soap, data, NULL, NULL) || soap_end_send(soap) )


SOAP_FMAC3 int SOAP_FMAC4 soap_put_ns1__sendPostResponse(struct soap*, const struct ns1__sendPostResponse *, const char*, const char*);

#define soap_read_ns1__sendPostResponse(soap, data) ( soap_begin_recv(soap) || !soap_get_ns1__sendPostResponse(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct ns1__sendPostResponse * SOAP_FMAC4 soap_get_ns1__sendPostResponse(struct soap*, struct ns1__sendPostResponse *, const char*, const char*);

#define soap_new_ns1__sendPostResponse(soap, n) ::soap_instantiate_ns1__sendPostResponse(soap, n, NULL, NULL, NULL)


#define soap_delete_ns1__sendPostResponse(soap, p) soap_delete(soap, p)

SOAP_FMAC1 struct ns1__sendPostResponse * SOAP_FMAC2 soap_instantiate_ns1__sendPostResponse(struct soap*, int, const char*, const char*, size_t*);
SOAP_FMAC3 void SOAP_FMAC4 soap_copy_ns1__sendPostResponse(struct soap*, int, int, void*, size_t, const void*, size_t);

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_PointerToSOAP_ENV__Reason
#define SOAP_TYPE_PointerToSOAP_ENV__Reason (25)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToSOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToSOAP_ENV__Reason(struct soap*, const char *, int, struct SOAP_ENV__Reason *const*, const char *);
SOAP_FMAC3 struct SOAP_ENV__Reason ** SOAP_FMAC4 soap_in_PointerToSOAP_ENV__Reason(struct soap*, const char*, struct SOAP_ENV__Reason **, const char*);

#define soap_write_PointerToSOAP_ENV__Reason(soap, data) ( soap_begin_send(soap) || (soap_serialize_PointerToSOAP_ENV__Reason(soap, data), 0) || soap_put_PointerToSOAP_ENV__Reason(soap, data, NULL, NULL) || soap_end_send(soap) )

SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToSOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason *const*, const char*, const char*);

#define soap_read_PointerToSOAP_ENV__Reason(soap, data) ( soap_begin_recv(soap) || !soap_get_PointerToSOAP_ENV__Reason(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct SOAP_ENV__Reason ** SOAP_FMAC4 soap_get_PointerToSOAP_ENV__Reason(struct soap*, struct SOAP_ENV__Reason **, const char*, const char*);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_PointerToSOAP_ENV__Detail
#define SOAP_TYPE_PointerToSOAP_ENV__Detail (24)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToSOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToSOAP_ENV__Detail(struct soap*, const char *, int, struct SOAP_ENV__Detail *const*, const char *);
SOAP_FMAC3 struct SOAP_ENV__Detail ** SOAP_FMAC4 soap_in_PointerToSOAP_ENV__Detail(struct soap*, const char*, struct SOAP_ENV__Detail **, const char*);

#define soap_write_PointerToSOAP_ENV__Detail(soap, data) ( soap_begin_send(soap) || (soap_serialize_PointerToSOAP_ENV__Detail(soap, data), 0) || soap_put_PointerToSOAP_ENV__Detail(soap, data, NULL, NULL) || soap_end_send(soap) )

SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToSOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail *const*, const char*, const char*);

#define soap_read_PointerToSOAP_ENV__Detail(soap, data) ( soap_begin_recv(soap) || !soap_get_PointerToSOAP_ENV__Detail(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct SOAP_ENV__Detail ** SOAP_FMAC4 soap_get_PointerToSOAP_ENV__Detail(struct soap*, struct SOAP_ENV__Detail **, const char*, const char*);

#endif

#ifndef WITH_NOGLOBAL

#ifndef SOAP_TYPE_PointerToSOAP_ENV__Code
#define SOAP_TYPE_PointerToSOAP_ENV__Code (18)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerToSOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerToSOAP_ENV__Code(struct soap*, const char *, int, struct SOAP_ENV__Code *const*, const char *);
SOAP_FMAC3 struct SOAP_ENV__Code ** SOAP_FMAC4 soap_in_PointerToSOAP_ENV__Code(struct soap*, const char*, struct SOAP_ENV__Code **, const char*);

#define soap_write_PointerToSOAP_ENV__Code(soap, data) ( soap_begin_send(soap) || (soap_serialize_PointerToSOAP_ENV__Code(soap, data), 0) || soap_put_PointerToSOAP_ENV__Code(soap, data, NULL, NULL) || soap_end_send(soap) )

SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerToSOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code *const*, const char*, const char*);

#define soap_read_PointerToSOAP_ENV__Code(soap, data) ( soap_begin_recv(soap) || !soap_get_PointerToSOAP_ENV__Code(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 struct SOAP_ENV__Code ** SOAP_FMAC4 soap_get_PointerToSOAP_ENV__Code(struct soap*, struct SOAP_ENV__Code **, const char*, const char*);

#endif

#ifndef SOAP_TYPE_PointerTons1__sendData
#define SOAP_TYPE_PointerTons1__sendData (12)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons1__sendData(struct soap*, ns1__sendData *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons1__sendData(struct soap*, const char *, int, ns1__sendData *const*, const char *);
SOAP_FMAC3 ns1__sendData ** SOAP_FMAC4 soap_in_PointerTons1__sendData(struct soap*, const char*, ns1__sendData **, const char*);

#define soap_write_PointerTons1__sendData(soap, data) ( soap_begin_send(soap) || (soap_serialize_PointerTons1__sendData(soap, data), 0) || soap_put_PointerTons1__sendData(soap, data, NULL, NULL) || soap_end_send(soap) )

SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons1__sendData(struct soap*, ns1__sendData *const*, const char*, const char*);

#define soap_read_PointerTons1__sendData(soap, data) ( soap_begin_recv(soap) || !soap_get_PointerTons1__sendData(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 ns1__sendData ** SOAP_FMAC4 soap_get_PointerTons1__sendData(struct soap*, ns1__sendData **, const char*, const char*);

#ifndef SOAP_TYPE_PointerTons1__responseData
#define SOAP_TYPE_PointerTons1__responseData (11)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_PointerTons1__responseData(struct soap*, ns1__responseData *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_PointerTons1__responseData(struct soap*, const char *, int, ns1__responseData *const*, const char *);
SOAP_FMAC3 ns1__responseData ** SOAP_FMAC4 soap_in_PointerTons1__responseData(struct soap*, const char*, ns1__responseData **, const char*);

#define soap_write_PointerTons1__responseData(soap, data) ( soap_begin_send(soap) || (soap_serialize_PointerTons1__responseData(soap, data), 0) || soap_put_PointerTons1__responseData(soap, data, NULL, NULL) || soap_end_send(soap) )

SOAP_FMAC3 int SOAP_FMAC4 soap_put_PointerTons1__responseData(struct soap*, ns1__responseData *const*, const char*, const char*);

#define soap_read_PointerTons1__responseData(soap, data) ( soap_begin_recv(soap) || !soap_get_PointerTons1__responseData(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 ns1__responseData ** SOAP_FMAC4 soap_get_PointerTons1__responseData(struct soap*, ns1__responseData **, const char*, const char*);

#ifndef SOAP_TYPE__QName
#define SOAP_TYPE__QName (5)
#endif

#define soap_default__QName(soap, a) soap_default_string(soap, a)


#define soap_serialize__QName(soap, a) soap_serialize_string(soap, a)

SOAP_FMAC3 int SOAP_FMAC4 soap_out__QName(struct soap*, const char*, int, char*const*, const char*);
SOAP_FMAC3 char * * SOAP_FMAC4 soap_in__QName(struct soap*, const char*, char **, const char*);

#define soap_write__QName(soap, data) ( soap_begin_send(soap) || (soap_serialize__QName(soap, data), 0) || soap_put__QName(soap, data, NULL, NULL) || soap_end_send(soap) )

SOAP_FMAC3 int SOAP_FMAC4 soap_put__QName(struct soap*, char *const*, const char*, const char*);

#define soap_read__QName(soap, data) ( soap_begin_recv(soap) || !soap_get__QName(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 char ** SOAP_FMAC4 soap_get__QName(struct soap*, char **, const char*, const char*);

#ifndef SOAP_TYPE_string
#define SOAP_TYPE_string (4)
#endif
SOAP_FMAC3 void SOAP_FMAC4 soap_default_string(struct soap*, char **);
SOAP_FMAC3 void SOAP_FMAC4 soap_serialize_string(struct soap*, char *const*);
SOAP_FMAC3 int SOAP_FMAC4 soap_out_string(struct soap*, const char*, int, char*const*, const char*);
SOAP_FMAC3 char * * SOAP_FMAC4 soap_in_string(struct soap*, const char*, char **, const char*);

#define soap_write_string(soap, data) ( soap_begin_send(soap) || (soap_serialize_string(soap, data), 0) || soap_put_string(soap, data, NULL, NULL) || soap_end_send(soap) )

SOAP_FMAC3 int SOAP_FMAC4 soap_put_string(struct soap*, char *const*, const char*, const char*);

#define soap_read_string(soap, data) ( soap_begin_recv(soap) || !soap_get_string(soap, data, NULL, NULL) || soap_end_recv(soap) )

SOAP_FMAC3 char ** SOAP_FMAC4 soap_get_string(struct soap*, char **, const char*, const char*);

#endif

/* End of soapH.h */
