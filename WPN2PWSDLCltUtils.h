/*
 *  WPN2PWSDLCltUtils.h
 *  WPN2P
 *
 *  Created by ferllanas on 1/27/16.
 *  Copyright 2016 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __WPN2PWSDLCltUtils_H_DEFINED__
#define __WPN2PWSDLCltUtils_H_DEFINED__

#include"IPMUnknown.h"

#include "WPN2PID.h"



class IWPN2PWSDLCltUtils:public IPMUnknown
{
public: 
	enum { kDefaultIID = IID_IWPN2PWSDLCLTUTILSINTERFACE };
	
	//virtual const bool16 realizaConneccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL)=0;

	
	virtual const bool16 sendPost(PMString URL, PMString publicacionIn, PMString notasidsIN)=0;
	
	
};
#endif